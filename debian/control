Source: golang-github-jmoiron-sqlx
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hpe.com>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-go-sql-driver-mysql-dev,
               golang-github-lib-pq-dev,
               golang-github-mattn-go-sqlite3-dev
Standards-Version: 3.9.8
Homepage: https://github.com/jmoiron/sqlx
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jmoiron-sqlx
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jmoiron-sqlx.git
XS-Go-Import-Path: github.com/jmoiron/sqlx

Package: golang-github-jmoiron-sqlx-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-go-sql-driver-mysql-dev,
         golang-github-lib-pq-dev,
         golang-github-mattn-go-sqlite3-dev
Description: General purpose extensions to Golang's database/sql library
 sqlx is a library which provides a set of extensions on Go's standard
 database/sql library. The sqlx versions of sql.DB, sql.TX, sql.Stmt,
 et al. all leave the underlying interfaces untouched, so that their
 interfaces are a superset on the standard ones.  This makes it relatively
 painless to integrate existing codebases using database/sql with sqlx.
 .
 Major additional concepts are:
 .
   * Marshal rows into structs (with embedded struct support), maps, and slices
   * Named parameter support including prepared statements
   * Get and Select to go quickly from query to struct/slice
